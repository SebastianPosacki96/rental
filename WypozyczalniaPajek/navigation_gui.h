#ifndef _NAVIGATION_GUI_H_
#define _NAVIGATION_GUI_H_
#include "mainwindow.h"
#include "gui_settings.h"
class NavigationGui :public QWidget
{
Q_OBJECT
public:
	NavigationGui(MainWindow* parent, QGroupBox* navigation_info);
	~NavigationGui();

signals:
	void go_to_rental_clicked();
	void go_to_settings_clicked();

public slots:
	void go_to_rental_menu();
	void go_to_settings_menu();

protected:
	MainWindow* m_pMainWindow;
	GuiSettings m_Settings;
	QGridLayout* m_navigation_layout;
};

#endif
