#include "rented_games_gui.h"

RentedGamesGui::RentedGamesGui(MainWindow* parent, QGroupBox* rented_games_info)
{
	m_pMainWindow = parent;

	QPushButton* returnGame_button = new QPushButton("Oddaj gre recznie");
	QPushButton* returnAllGames_button = new QPushButton("Oddaj recznie wszystkie gry");

	returnGame_button->setMinimumSize(m_Settings.m_buttonWidthM, m_Settings.m_buttonHeightM);
	returnAllGames_button->setMinimumSize(m_Settings.m_buttonWidthM, m_Settings.m_buttonHeightM);

	returnGame_button->setFont(m_Settings.m_textFont);
	returnAllGames_button->setFont(m_Settings.m_textFont);
	rented_games_info->setFont(m_Settings.m_headerFont);

	m_rentedGames_list = new QTreeWidget;
	m_rentedGames_list->setColumnCount(4);
	m_rentedGames_list->setRootIsDecorated(false);
	QStringList games_headers;
	games_headers << tr("Nr.");
	games_headers << tr("Nazwa gry");
	games_headers << tr("Imie i nazwisko");
	games_headers << tr("Kod uzytkownika");
	m_rentedGames_list->setHeaderLabels(games_headers);

	m_rentedGames_layout = new QGridLayout(rented_games_info);

	m_rentedGames_layout->addWidget(m_rentedGames_list, 0, 0, 4, 2);
	m_rentedGames_layout->addWidget(returnGame_button, 4, 0, 1, 1);
	m_rentedGames_layout->addWidget(returnAllGames_button, 4, 1, 1, 1);
}

RentedGamesGui::~RentedGamesGui()
{

}