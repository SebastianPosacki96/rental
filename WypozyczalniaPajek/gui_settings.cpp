#include "gui_settings.h"

GuiSettings::GuiSettings()
{
	setFontPar();
	setButtonMinSize();
	setHeaderPar();
	setMaxImgSize();
	setMaxLogoSize();
}

GuiSettings::~GuiSettings()
{

}

void GuiSettings::setFontPar(QString textFont, int textSize)
{
	m_textFont.setFamily(textFont);
	m_textFont.setPointSize(textSize);
	m_textFont.setBold(0);
}

void GuiSettings::setHeaderPar(QString textFont, int textSize)
{
	m_headerFont.setFamily(textFont);
	m_headerFont.setPointSize(textSize);
	m_headerFont.setBold(0);
}

void GuiSettings::setButtonMinSize(int mWidth, int mHeight)
{
	m_buttonWidthM = mWidth;
	m_buttonHeightM = mHeight;
}

void GuiSettings::setMaxImgSize(int maxSize)
{
	m_maxImageSize = maxSize;
}

void GuiSettings::setMaxLogoSize(int maxW, int maxH)
{
	m_maxLogoSizeW = maxW;
	m_maxLogoSizeH = maxH;
}

void GuiSettings::scaleImage(QPixmap& image)
{
	int imgW = image.width();
	int imgH = image.height();
	double scale;
	if (imgW >= imgH)
	{
		scale = double(imgH) / double(imgW);
		imgW = m_maxImageSize;
		imgH = imgW * scale;
	}
	else
	{
		scale = double(imgW) / double(imgH);
		imgH = m_maxImageSize;
		imgW = imgH * scale;
	}
	QPixmap image_scaled = image.scaled(QSize(imgW, imgH));
	image = image_scaled;
}

void GuiSettings::scaleLogo(QPixmap& logo)
{

	int imgW = logo.width();
	int imgH = logo.height();
	double scale;
	if (imgW >= imgH)
	{
		scale = double(imgH) / double(imgW);
		imgW = m_maxImageSize;
		imgH = imgW * scale;
	}
	else
	{
		scale = double(imgW) / double(imgH);
		imgH = m_maxImageSize;
		imgW = imgH * scale;
	}
	QPixmap image_scaled = logo.scaled(QSize(imgW, imgH));
	logo = image_scaled;
}