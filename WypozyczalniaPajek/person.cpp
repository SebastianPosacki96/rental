#include "person.h"

Person::Person(int code, std::string name, std::string city, int age)
{
	m_code = code;
	m_name = name;
	m_city = city;
	m_age = age;
	m_gameId = -1;
	m_haveGame = 0;
}

Person::~Person()
{

}

void Person::addGame(int gameID)
{
	m_haveGame = true;
	m_gameId = gameID;
}

void Person::removeGame()
{
	m_haveGame = false;
	m_gameId = -1;
}