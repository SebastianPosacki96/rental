#ifndef _DATA_INTERFACE_H_
#define _DATA_INTERFACE_H_
#include <unordered_map>
#include <map>
#include "person.h"
#include "game.h"
#include <QStringList>
class DataInterface
{
public:
	DataInterface();
	~DataInterface();
	void addPerson(int code, Person personToAdd);
	void addGame(int id, Game gameToAdd);
	int gameMapSize();
	void fillWordList();
	void giveWordList(QStringList& wordList);
	void removeWordFromList(QString wordToRemove);
	bool findOnList(QString wordToFinde);
	bool checkIfRegistered(int code);
	bool doPersonHaveGame(int code);
	void addGameToPerson(int personID, int gameID);
	int findeGame(std::string gameName);
	void removeGameFromPerson(int personID, QString& gameName);
	QStringList m_wordList;
	std::string giveGameName(int gameID);
	//void showAll();
	Person returnPerson(int code);
	bool doesCodeExist(int code);
private:
	std::unordered_map<int, Person> m_registeredPerson;
	std::map<int, Game> m_gamesToRent;
};

#endif
