#ifndef _GAME_H_
#define _GAME_H_
#include <string>
class Game
{
public:
	Game(int gameId, std::string gameName);
	~Game();
	std::string getGameName() { return m_gameName; };
	bool isAvable() { return m_isAvable; };
	void rentGame();
	void returnGame();
private:
	int m_gameId;
	std::string m_gameName;
	bool m_isAvable;
};
#endif
