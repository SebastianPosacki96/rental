#include "game.h"

Game::Game(int gameId, std::string gameName)
{
	m_gameId = gameId;
	m_gameName = gameName;
	m_isAvable = 1;
}

Game::~Game()
{

}

void Game::rentGame()
{
	m_isAvable = 0;
}

void Game::returnGame()
{
	m_isAvable = 1;
}