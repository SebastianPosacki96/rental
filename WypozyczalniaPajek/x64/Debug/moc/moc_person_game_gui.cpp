/****************************************************************************
** Meta object code from reading C++ file 'person_game_gui.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../person_game_gui.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'person_game_gui.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PersonGameGui_t {
    QByteArrayData data[12];
    char stringdata0[110];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PersonGameGui_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PersonGameGui_t qt_meta_stringdata_PersonGameGui = {
    {
QT_MOC_LITERAL(0, 0, 13), // "PersonGameGui"
QT_MOC_LITERAL(1, 14, 13), // "checkUserCode"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 4), // "int&"
QT_MOC_LITERAL(4, 34, 6), // "userID"
QT_MOC_LITERAL(5, 41, 10), // "returnDone"
QT_MOC_LITERAL(6, 52, 11), // "refGameList"
QT_MOC_LITERAL(7, 64, 8), // "gameName"
QT_MOC_LITERAL(8, 73, 10), // "returnGame"
QT_MOC_LITERAL(9, 84, 8), // "showGame"
QT_MOC_LITERAL(10, 93, 6), // "gameID"
QT_MOC_LITERAL(11, 100, 9) // "cleanGame"

    },
    "PersonGameGui\0checkUserCode\0\0int&\0"
    "userID\0returnDone\0refGameList\0gameName\0"
    "returnGame\0showGame\0gameID\0cleanGame"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PersonGameGui[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x06 /* Public */,
       5,    0,   47,    2, 0x06 /* Public */,
       6,    1,   48,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   51,    2, 0x0a /* Public */,
       9,    1,   52,    2, 0x0a /* Public */,
      11,    0,   55,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    7,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void,

       0        // eod
};

void PersonGameGui::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<PersonGameGui *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->checkUserCode((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->returnDone(); break;
        case 2: _t->refGameList((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->returnGame(); break;
        case 4: _t->showGame((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->cleanGame(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (PersonGameGui::*)(int & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PersonGameGui::checkUserCode)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (PersonGameGui::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PersonGameGui::returnDone)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (PersonGameGui::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PersonGameGui::refGameList)) {
                *result = 2;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PersonGameGui::staticMetaObject = { {
    QMetaObject::SuperData::link<QWidget::staticMetaObject>(),
    qt_meta_stringdata_PersonGameGui.data,
    qt_meta_data_PersonGameGui,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PersonGameGui::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PersonGameGui::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PersonGameGui.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int PersonGameGui::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void PersonGameGui::checkUserCode(int & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void PersonGameGui::returnDone()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void PersonGameGui::refGameList(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
