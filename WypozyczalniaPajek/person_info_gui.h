#ifndef _PERSON_INFO_GUI_H_
#define _PERSON_INFO_GUI_H_
#include "mainwindow.h"
#include "gui_settings.h"

class PersonInfoGui :public QWidget
{
    Q_OBJECT
public:
    PersonInfoGui(MainWindow* parent, QGroupBox* person_info);
    ~PersonInfoGui();
    void giveCode(QString &code);

signals:
    void codeChnged();

public slots:
    void registerClicked();
    void searchClicked();
    void cleanLineEdits();
    void personCodeChanged();

protected:
    MainWindow* m_pMainWindow;
    GuiSettings m_Settings;
    QGridLayout* m_person_layout;
    QLineEdit* m_code_line_edit;
    QLineEdit* m_user_line_edit;
};
#endif