#ifndef _RENTAL_GUI_H_
#define _RENTAL_GUI_H_
#include "mainwindow.h"
#include "gui_settings.h"
#include <QStringList>
class RentalGui :public QWidget
{
public:
	RentalGui(MainWindow* parent, QGroupBox* rental_info);
	~RentalGui();
	void setNoFoto();
	void refWordList(QString gameName);
	void changeGameFoto(QString imageName);

public slots:
	void rentGame();
	void showGame();

protected:
	MainWindow* m_pMainWindow;
	GuiSettings m_Settings;
	QGridLayout* m_rental_layout;
	QLabel* m_imageLabel;
	QCompleter* m_gameCompleter;
	QLineEdit* m_games_line_edit;
};

#endif