#include "person_info_gui.h"

PersonInfoGui::PersonInfoGui(MainWindow* parent, QGroupBox* person_info)
{
	m_pMainWindow = parent;
	QLabel* user_label = new QLabel("Imie i nazwisko: ");
	QLabel* code_label = new QLabel("Kod: ");

	m_code_line_edit = new QLineEdit();
	m_user_line_edit = new QLineEdit();

	QPushButton* search_button = new QPushButton("Szukaj");
	QPushButton* register_button = new QPushButton("Rejestracja");

	search_button->setMinimumSize(m_Settings.m_buttonWidthM, m_Settings.m_buttonHeightM);
	register_button->setMinimumSize(m_Settings.m_buttonWidthM, m_Settings.m_buttonHeightM);

	connect(search_button, &QPushButton::clicked, this, &PersonInfoGui::searchClicked);
	connect(register_button, &QPushButton::clicked, this, &PersonInfoGui::registerClicked);
	connect(m_code_line_edit, &QLineEdit::textChanged, this, &PersonInfoGui::personCodeChanged);
	connect(this, &PersonInfoGui::codeChnged, m_pMainWindow, &MainWindow::personCodeChanged);

	user_label->setFont(m_Settings.m_textFont);
	code_label->setFont(m_Settings.m_textFont);
	register_button->setFont(m_Settings.m_textFont);
	search_button->setFont(m_Settings.m_textFont);
	m_code_line_edit->setFont(m_Settings.m_textFont);
	m_user_line_edit->setFont(m_Settings.m_textFont);
	person_info->setFont(m_Settings.m_headerFont);

	m_person_layout = new QGridLayout(person_info);

	m_person_layout->addWidget(code_label, 0, 0);
	m_person_layout->addWidget(user_label, 1, 0);
	m_person_layout->addWidget(m_code_line_edit, 0, 1, 1, 3);
	m_person_layout->addWidget(m_user_line_edit, 1, 1, 1, 3);
	m_person_layout->addWidget(search_button, 2, 0, 2, 2);
	m_person_layout->addWidget(register_button, 2, 2, 2, 2);
}


PersonInfoGui::~PersonInfoGui()
{

}

void PersonInfoGui::searchClicked()
{
	if (m_code_line_edit->text() == "")
	{
		QMessageBox msgBox;
		msgBox.setWindowTitle("Wyszukiwanie nieudane");
		msgBox.setText("Nie podano kodu");
		msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
		int ret = msgBox.exec();
		return;
	}
	int code = stoi(m_code_line_edit->text().toStdString());
	if (!m_pMainWindow->m_myData->doesCodeExist(code))
	{
		QMessageBox msgBox;
		msgBox.setWindowTitle("Wyszukiwanie nieudane");
		msgBox.setText("Podany kod nie istnieje");
		msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
		int ret = msgBox.exec();
		return;
	}
	Person personFound = m_pMainWindow->m_myData->returnPerson(code);
	m_user_line_edit->setText(QString::fromStdString(personFound.getName()));
	if (personFound.doesHaveGame())
	{
		m_pMainWindow->showGame(personFound.giveGameID());
	}

}

void PersonInfoGui::registerClicked()
{
	if (m_code_line_edit->text() == "")
	{
		QMessageBox msgBox;
		msgBox.setWindowTitle("Rejestracja nieudana");
		msgBox.setText("Nie podano kodu");
		msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
		int ret = msgBox.exec();
		return;
	}
	if (m_user_line_edit->text() == "")
	{
		QMessageBox msgBox;
		msgBox.setWindowTitle("Rejestracja nieudana");
		msgBox.setText("Nie podano imienia");
		msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
		int ret = msgBox.exec();
		return;
	}
	int code = stoi(m_code_line_edit->text().toStdString());
	std::string name = m_user_line_edit->text().toStdString();
	if (m_pMainWindow->m_myData->doesCodeExist(code))
	{
		QMessageBox msgBox;
		msgBox.setWindowTitle("Rejestracja nieudana");
		msgBox.setText("Podany kod juz istnieje");
		msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
		int ret = msgBox.exec();
		return;
	}
	Person personToAdd(code, name);
	m_pMainWindow->m_myData->addPerson(code, personToAdd);
}

void PersonInfoGui::giveCode(QString& code)
{
	code = m_code_line_edit->text();
}

void PersonInfoGui::cleanLineEdits()
{
	m_code_line_edit->clear();
	m_user_line_edit->clear();
}

void PersonInfoGui::personCodeChanged()
{
	m_user_line_edit->clear();
	emit codeChnged();
}