#ifndef _RENTED_GAMES_GUI_H_
#define _RENTED_GAMES_GUI_H_

#include "mainwindow.h"
#include "gui_settings.h"
#include <QtWidgets>

class RentedGamesGui :public QWidget
{
public:
	RentedGamesGui(MainWindow* parent, QGroupBox* rented_games_info);
	~RentedGamesGui();

private:
	MainWindow* m_pMainWindow;
	QGridLayout* m_rentedGames_layout;
	GuiSettings m_Settings;
	QTreeWidget* m_rentedGames_list{ 0 };
};

#endif