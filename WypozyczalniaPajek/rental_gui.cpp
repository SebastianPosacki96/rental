#include "rental_gui.h"
#include <QCompleter>
#include <QStringListModel>
RentalGui::RentalGui(MainWindow* parent, QGroupBox* rental_info)
{
	m_pMainWindow = parent;
	QLabel* game_return_label = new QLabel("Nazwa gry: ");
	
	QPixmap image("../games/nofoto");
	m_Settings.scaleImage(image);
	m_imageLabel = new QLabel();
	m_imageLabel->setPixmap(image);

	m_games_line_edit = new QLineEdit();
	m_gameCompleter = new QCompleter(m_pMainWindow->m_myData->m_wordList);
	m_gameCompleter->setCaseSensitivity(Qt::CaseInsensitive);
	m_games_line_edit->setCompleter(m_gameCompleter);

	QPushButton* lend_game_button = new QPushButton("Wypozycz gre");

	lend_game_button->setMinimumSize(m_Settings.m_buttonWidthM, m_Settings.m_buttonHeightM);

	connect(lend_game_button, &QPushButton::clicked, this, &RentalGui::rentGame);
	connect(m_games_line_edit, &QLineEdit::textChanged, this, &RentalGui::showGame);

	game_return_label->setFont(m_Settings.m_textFont);
	m_games_line_edit->setFont(m_Settings.m_textFont);
	lend_game_button->setFont(m_Settings.m_textFont);
	rental_info->setFont(m_Settings.m_headerFont);

	m_rental_layout = new QGridLayout(rental_info);

	m_rental_layout->addWidget(game_return_label, 0, 0);
	m_rental_layout->addWidget(m_games_line_edit, 0, 1, 1, 4);
	m_rental_layout->addWidget(lend_game_button, 1, 0, 1, 5);
	m_rental_layout->addWidget(m_imageLabel, 2, 0, 1, 5, Qt::AlignCenter);
}

RentalGui::~RentalGui()
{

}

void RentalGui::rentGame()
{
	QString gameName = m_games_line_edit->text();
	if (gameName.size() == 0)
	{
		m_pMainWindow->giveFailMsg("Wypozyczanie nieudane", "Nie podano nazwy gry");
		return;
	}
	if (!m_pMainWindow->m_myData->findOnList(gameName))
	{
		m_pMainWindow->giveFailMsg("Wypozyczanie nieudane", "Nie znaleziono gry");
		return;
	}
	std::string gameNameToGive = gameName.toStdString();
	if (!m_pMainWindow->giveGameIfCode(gameNameToGive))
	{
		return;
	}
	m_pMainWindow->m_myData->removeWordFromList(gameName);
	refWordList(gameName);
	m_games_line_edit->clear();
	m_pMainWindow->rentDone();
	setNoFoto();
}

void RentalGui::refWordList(QString gameName)
{
	QStringList afterSort = m_pMainWindow->m_myData->m_wordList;
	afterSort.sort(Qt::CaseInsensitive);
	m_gameCompleter->setModel(new QStringListModel(afterSort, m_gameCompleter));
}

void RentalGui::setNoFoto()
{
	QPixmap image("../games/nofoto");
	m_Settings.scaleImage(image);
	m_imageLabel->setPixmap(image);
}

void  RentalGui::changeGameFoto(QString imageName)
{
	imageName = "../games/" + imageName;
	QPixmap image(imageName);
	if (image.isNull())
	{
		image.load("../games/nogamefoto");
	}
	m_Settings.scaleImage(image);
	m_imageLabel->setPixmap(image);
}

void RentalGui::showGame()
{
	QString gameName;
	gameName = m_games_line_edit->text();
	if (gameName == "")
	{
		setNoFoto();
	}
	else if (m_pMainWindow->m_myData->findOnList(gameName))
	{
		changeGameFoto(gameName);
	}
	else
	{
		changeGameFoto("nogamefoto");
	}
	return;

}