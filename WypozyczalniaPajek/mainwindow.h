#ifndef _MAINWINDOW_H_
#define _MAINWINDOW_H_
#include <QtWidgets/QWidget>
#include <QGridLayout>
#include <QLabel.h>
#include <qpushbutton.h>
#include <qlineedit.h>
#include <qgroupbox.h>
#include <qpixmap.h>
#include <qstackedwidget.h>
#include <QMessageBox>
#include "data_interface.h"

class PersonInfoGui;
class PersonGameGui;
class RentalGui;
class NavigationGui;
class RentedGamesGui;
class MainWindow : public QWidget
{
    Q_OBJECT
public:
    MainWindow(DataInterface* myData);
    ~MainWindow();
    QStackedWidget* m_pages_stacked;
    DataInterface* m_myData;
    bool giveGameIfCode(std::string gameName);
    void giveFailMsg(QString header, QString message);

signals:
    void rentDone();
    void returnDone();
    void showGame(int gameID);
    void personCodeChanged();

public slots:
    void go_to_settings_menu();
    void go_to_rental_menu();
    void setSize();
    void findeUser(int &userID);
    void addWordToList(QString gameName);

protected:
    QWidget* m_Parent;
    QWidget* m_rentalPage;
    QGridLayout* m_open_layout;
    PersonInfoGui* m_personGui;
    PersonGameGui* m_gameGui;
    RentalGui* m_rentalGui;
    NavigationGui* m_navigationGui;

    QWidget* m_settingsPage;
    QGridLayout* m_settings_layout;
    NavigationGui* m_navigationGui2;
    RentedGamesGui* m_rentedGamesGui;
};
#endif