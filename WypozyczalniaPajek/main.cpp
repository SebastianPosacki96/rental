#include "mainwindow.h"
#include <QtWidgets/QApplication>
#include <qstackedwidget.h>
#include "control_panel.h"
#include <fstream>

void readGamesFromFile(DataInterface* myData)
{
    std::ifstream myfile("games.csv");
    if (!myfile.is_open())
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Wczytywanie nieudane");
        msgBox.setText("Nie udalo sie wczytac gier z pliku, sprawdz plik");
        msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        int ret = msgBox.exec();
        return;
    }
    int gameId;
    std::string gameName;
    std::getline(myfile, gameName, '\n');
    Game tmp_game(0,"");
    while (myfile.good())
    {
        std::getline(myfile, gameName, '\n');
        if (gameName == "")
        {
            break;
        }
        gameId = myData->gameMapSize()+1;
        tmp_game = Game(gameId, gameName);
        myData->addGame(gameId,tmp_game);
    }
}
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    DataInterface* myData = new DataInterface;
    readGamesFromFile(myData);
    myData->fillWordList();
    QWidget* layout = new QWidget;
    MainWindow mainLayout2(myData);
    QHBoxLayout* grid_layout = new QHBoxLayout(layout);
    grid_layout->addWidget(mainLayout2.m_pages_stacked, 0, 0);
    layout->setWindowState(layout->windowState() ^ Qt::WindowMaximized);
    layout->setMinimumSize(1400,1000);
    layout->show();

    //myData->showAll();
    return a.exec();
}
