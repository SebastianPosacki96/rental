#include <iostream>
#include "data_interface.h"
#include <QMessageBox>
#include <string>
DataInterface::DataInterface()
{

}

DataInterface::~DataInterface()
{

}

void DataInterface::addPerson(int code, Person personToAdd)
{
	m_registeredPerson.insert(std::pair<int, Person>(code, personToAdd));
}

bool DataInterface::doesCodeExist(int code)
{
    if (m_registeredPerson.find(code) == m_registeredPerson.end())
    {
        return false;
    }
    return true;
}

Person DataInterface::returnPerson(int code)
{
    return(m_registeredPerson.at(code));
}

bool DataInterface::checkIfRegistered(int code)
{
    if (m_registeredPerson.find(code) == m_registeredPerson.end())
    {
        return false;
    }
    return true;
}

void DataInterface::addGame(int id, Game gameToAdd)
{
    m_gamesToRent.insert(std::pair<int, Game>(id, gameToAdd));
}

int DataInterface::gameMapSize()
{
    return m_gamesToRent.size();
}

void DataInterface::fillWordList()
{
    for (int i = 1; i <= m_gamesToRent.size(); i++)
    {
        m_wordList << QString::fromStdString(m_gamesToRent.at(i).getGameName());
    }
    m_wordList.sort(Qt::CaseInsensitive);
}

void DataInterface::giveWordList(QStringList& wordList)
{
    wordList = m_wordList;
    wordList.sort();
}

void DataInterface::removeWordFromList(QString wordToRemove)
{
    int index;
    QString gameName;
    for (int i = 0; i < m_wordList.size(); i++)
    {
        gameName = m_wordList[i];
        index = i;
        if (gameName.contains(wordToRemove, Qt::CaseInsensitive))
        {
            break;
        }
    }
    m_wordList.removeAt(index);
    int a = 9;
}

bool DataInterface::findOnList(QString wordToFinde)
{
    return m_wordList.contains(wordToFinde, Qt::CaseInsensitive);
}

bool DataInterface::doPersonHaveGame(int code)
{
    return m_registeredPerson.at(code).doesHaveGame();
}

int DataInterface::findeGame(std::string gameName)
{
    int index = -1;
    std::string gameNameInList;
    for (auto& c : gameName) c = toupper(c);
    for (auto const& pair : m_gamesToRent)
    {
        index = pair.first;
        gameNameInList = m_gamesToRent.at(index).getGameName();
        for (auto& c : gameNameInList) c = toupper(c);
        if (gameNameInList == gameName)
        {
            if (m_gamesToRent.at(index).isAvable())
            {
                return index;
            }
        }
        index = -1;
    }
    return index;
}

void DataInterface::addGameToPerson(int personID, int gameID)
{
    m_registeredPerson.at(personID).addGame(gameID);
    m_gamesToRent.at(gameID).rentGame();
}

std::string DataInterface::giveGameName(int gameID)
{
    return m_gamesToRent.at(gameID).getGameName();
}

void DataInterface::removeGameFromPerson(int personID, QString &gameName)
{
    int gameID = m_registeredPerson.at(personID).giveGameID();
    m_registeredPerson.at(personID).removeGame();
    m_gamesToRent.at(gameID).returnGame();
    gameName = QString::fromStdString(m_gamesToRent.at(gameID).getGameName());
    m_wordList.push_back(gameName);
}