#ifndef _PERSON_GAME_GUI_H_
#define _PERSON_GAME_GUI_H_
#include "mainwindow.h"
#include "gui_settings.h"

class PersonGameGui :public QWidget
{
Q_OBJECT
public:
	PersonGameGui(MainWindow* parent, QGroupBox* person_info);
	~PersonGameGui();
	void changeGame(QString);

signals:
	void checkUserCode(int &userID);
	void returnDone();
	void refGameList(QString gameName);

public slots:
	void returnGame();
	void showGame(int gameID);
	void cleanGame();
protected:
	MainWindow* m_pMainWindow;
	GuiSettings m_Settings;
	QLineEdit* m_game_line_edit;
	QGridLayout* m_game_layout;
	QLabel* m_imageLabel;
};
#endif