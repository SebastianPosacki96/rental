#include "mainwindow.h"
#include "person_info_gui.h"
#include "person_game_gui.h"
#include "rental_gui.h"
#include "navigation_gui.h"
#include "rented_games_gui.h"

MainWindow::MainWindow(DataInterface* myData)
{
    m_myData = myData;
    m_Parent = new QWidget;
    m_pages_stacked = new QStackedWidget(m_Parent);
    m_rentalPage = new QWidget;
    m_open_layout = new QGridLayout(m_rentalPage);

    QGroupBox* person_info = new QGroupBox("Uzytkownik:");
    m_personGui = new PersonInfoGui(this, person_info);

    QGroupBox* person_game = new QGroupBox("Lista gier:");
    m_gameGui = new PersonGameGui(this, person_game);

    QGroupBox* game_list = new QGroupBox("Lista gier:");
    m_rentalGui = new RentalGui(this, game_list);

    QGroupBox* navigation = new QGroupBox("Nawigacja:");
    m_navigationGui = new NavigationGui(this, navigation);

    m_open_layout->setColumnStretch(0, 1);
    m_open_layout->setColumnStretch(1, 1);
    m_open_layout->setColumnStretch(2, 3);

    m_open_layout->addWidget(person_info, 0, 0, 2, 2);
    m_open_layout->addWidget(person_game, 2, 0, 4, 2);
    m_open_layout->addWidget(game_list, 0, 2, 5, 2);
    m_open_layout->addWidget(navigation, 5, 2, 2, 2);
   
    connect(this, &MainWindow::rentDone, m_personGui, &PersonInfoGui::cleanLineEdits);
    connect(this, &MainWindow::returnDone, m_personGui, &PersonInfoGui::cleanLineEdits);
    connect(this, &MainWindow::showGame, m_gameGui, &PersonGameGui::showGame);
    connect(this, &MainWindow::personCodeChanged, m_gameGui, &PersonGameGui::cleanGame);

    m_settingsPage = new QWidget;
    m_settings_layout = new QGridLayout(m_settingsPage);
    QGroupBox* navigation2 = new QGroupBox("Nawigacja:");

    QGroupBox* rentedGames = new QGroupBox("Aktualnie wypozyczone gry:");
    m_rentedGamesGui = new RentedGamesGui(this, rentedGames);

    m_navigationGui2 = new NavigationGui(this, navigation2);
    m_settings_layout->setColumnStretch(0, 3);
    m_settings_layout->setColumnStretch(1, 1);
    m_settings_layout->addWidget(rentedGames, 0, 0, 2, 1);
    m_settings_layout->addWidget(navigation2, 1, 1, 1, 1);

    m_pages_stacked->addWidget(m_rentalPage);
    m_pages_stacked->addWidget(m_settingsPage);
}

MainWindow::~MainWindow()
{
}

void MainWindow::go_to_settings_menu()
{
    m_pages_stacked->setCurrentIndex(1);
}

void MainWindow::go_to_rental_menu()
{
    m_pages_stacked->setCurrentIndex(0);
}

void MainWindow::setSize()
{
    m_pages_stacked->setFixedSize(m_Parent->width(), m_Parent->height());
}

bool MainWindow::giveGameIfCode(std::string gameName)
{
    QString personCode;
    m_personGui->giveCode(personCode);
    if (personCode.size() == 0)
    {
        giveFailMsg("Wypozyczanie nieudane", "Nie podano kodu");
        return false;
    }
    int personCodeNumeric = stoi(personCode.toStdString());
    if (!m_myData->checkIfRegistered(personCodeNumeric))
    {
        giveFailMsg("Wypozyczanie nieudane", "Nie znaleziono uzytkownika");
        return false;
    }
    if (m_myData->doPersonHaveGame(personCodeNumeric))
    {
        giveFailMsg("Wypozyczanie nieudane", "Uzytkownik juz posiada gre");
        return false;
    }
    int gameID = m_myData->findeGame(gameName);
    if (gameID == -1)
    {
        giveFailMsg("Wypozyczanie nieudane", "Nie odnaleziono gry");
        return false;
    }
    m_myData->addGameToPerson(personCodeNumeric, gameID);
    return true;
}

void MainWindow::giveFailMsg(QString header, QString message)
{
    QMessageBox msgBox;
    msgBox.setWindowTitle(header);
    msgBox.setText(message);
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    int ret = msgBox.exec();
}

void MainWindow::findeUser(int& userID)
{
    QString personID;
    m_personGui->giveCode(personID);
    if (personID.size() == 0)
    {
        giveFailMsg("Oddawanie nieudane", "Nie podano kodu uzytkownika");
        return;
    }
    userID = std::stoi(personID.toStdString());
    if (!m_myData->checkIfRegistered(userID))
    {
        giveFailMsg("Oddawanie nieudane", "Nie znaleziono uzytkownika o podanym kodzie");
        userID = -1;
        return;
    }

}

void MainWindow::addWordToList(QString gameName)
{
    m_rentalGui->refWordList(gameName);
}