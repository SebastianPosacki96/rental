#ifndef _GUI_SETTINGS_H_
#define _GUI_SETTINGS_H_
#include "mainwindow.h"

class GuiSettings :public QWidget
{
public:
    GuiSettings();
    ~GuiSettings();
    void setFontPar(QString textFont = "Times", int textSize = 20);
    void setHeaderPar(QString textFont = "Times", int textSize = 8);
    void setButtonMinSize(int mWidth=100, int mHeight=70);
    void setMaxImgSize(int maxSize = 450);
    void setMaxLogoSize(int maxW = 450, int maxH = 450);
    void scaleImage(QPixmap& image);
    void scaleLogo(QPixmap& logo);

public:
    QFont m_textFont;
    QFont m_headerFont;
    int m_buttonWidthM;
    int m_buttonHeightM;
    int m_maxImageSize;
    int m_maxLogoSizeW;
    int m_maxLogoSizeH;
};

#endif
