#include "person_game_gui.h"

PersonGameGui::PersonGameGui(MainWindow* parent, QGroupBox* game_info)
{
	m_pMainWindow = parent;

	QLabel* game_label = new QLabel("Nazwa gry: ");

	QPixmap image("../games/nofoto");
	m_Settings.scaleImage(image);
	m_imageLabel = new QLabel();
	m_imageLabel->setPixmap(image);

	QPixmap image_logo("logo.png");
	m_Settings.scaleLogo(image_logo);
	QLabel* image_of_logo = new QLabel();
	image_of_logo->setPixmap(image_logo);

	m_game_line_edit = new QLineEdit();
	m_game_line_edit->setReadOnly(true);

	QPushButton* return_game_button = new QPushButton("Oddaj gre");

	return_game_button->setMinimumSize(m_Settings.m_buttonWidthM, m_Settings.m_buttonHeightM);

	game_label->setFont(m_Settings.m_textFont);
	return_game_button->setFont(m_Settings.m_textFont);
	m_game_line_edit->setFont(m_Settings.m_textFont);
	game_info->setFont(m_Settings.m_headerFont);
	
	connect(return_game_button, &QPushButton::clicked, this, &PersonGameGui::returnGame);
	connect(this, &PersonGameGui::checkUserCode, m_pMainWindow, &MainWindow::findeUser);
	connect(this, &PersonGameGui::returnDone, m_pMainWindow, &MainWindow::returnDone);
	connect(this, &PersonGameGui::refGameList, m_pMainWindow, &MainWindow::addWordToList);

	m_game_layout = new QGridLayout(game_info);

	m_game_layout->addWidget(game_label, 0, 0);
	m_game_layout->addWidget(m_game_line_edit, 0, 1, 1, 3);
	m_game_layout->addWidget(return_game_button, 1, 0, 1, 4);
	m_game_layout->addWidget(m_imageLabel, 2, 0, 1, 2, Qt::AlignCenter);
	m_game_layout->addWidget(image_of_logo, 2, 2, 1, 2, Qt::AlignCenter);
}

PersonGameGui::~PersonGameGui()
{

}

void PersonGameGui::changeGame(QString imageName)
{
	QPixmap image(imageName);
	if (image.isNull())
	{
		image.load("../games/nogamefoto");
	}
	m_Settings.scaleImage(image);
	m_imageLabel->setPixmap(image);
}

void PersonGameGui::showGame(int gameID)
{
	QString gameName = QString::fromStdString(m_pMainWindow->m_myData->giveGameName(gameID));
	m_game_line_edit->setText(gameName);
	gameName = "../games/" + gameName;
	changeGame(gameName);
}

void PersonGameGui::cleanGame()
{
	m_game_line_edit->clear();
	changeGame("../games/nofoto");
}

void PersonGameGui::returnGame()
{
	int userID = -1;
	emit checkUserCode(userID);
	if (userID == -1)
	{
		return;
	}
	if (!m_pMainWindow->m_myData->doPersonHaveGame(userID))
	{
		m_pMainWindow->giveFailMsg("Oddawanie nieudane", "Uzytkownik nie posiada gry");
		return;
	}
	cleanGame();
	QString gameName;
	m_pMainWindow->m_myData->removeGameFromPerson(userID, gameName);
	emit returnDone();
	emit refGameList(gameName);
}