#ifndef _PERSON_H_
#define _PERSON_H_
#include <string>
class Person
{
public:
	Person(int code, std::string name, std::string city = "", int age =0);
	~Person();
	std::string getName() { return m_name; };
	bool doesHaveGame() { return m_haveGame; };
	void addGame(int gameID);
	void removeGame();
	int giveGameID() { return m_gameId; };

private:
	std::string m_name;
	std::string m_city;
	int m_age;
	int m_code;
	bool m_haveGame;
	int m_gameId;
};

#endif