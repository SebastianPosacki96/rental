#include "navigation_gui.h"
NavigationGui::NavigationGui(MainWindow* parent, QGroupBox* navigation_info)
{
	m_pMainWindow = parent;

	QPushButton* rental_menu_button = new QPushButton("Wypozyczalnia");
	QPushButton* settings_button = new QPushButton("Ustawienia");

	rental_menu_button->setMinimumSize(m_Settings.m_buttonWidthM, m_Settings.m_buttonHeightM);
	settings_button->setMinimumSize(m_Settings.m_buttonWidthM, m_Settings.m_buttonHeightM);

	rental_menu_button->setFont(m_Settings.m_textFont);
	settings_button->setFont(m_Settings.m_textFont);
	navigation_info->setFont(m_Settings.m_headerFont);

	connect(rental_menu_button, &QPushButton::clicked, this, &NavigationGui::go_to_rental_menu);
	connect(settings_button, &QPushButton::clicked, this, &NavigationGui::go_to_settings_menu);
	connect(this, &NavigationGui::go_to_rental_clicked, m_pMainWindow, &MainWindow::go_to_rental_menu);
	connect(this, &NavigationGui::go_to_settings_clicked, m_pMainWindow, &MainWindow::go_to_settings_menu);


	m_navigation_layout = new QGridLayout(navigation_info);

	m_navigation_layout->addWidget(rental_menu_button, 0, 0, 2, 1);
	m_navigation_layout->addWidget(settings_button, 0, 2, 2, 1);
}

NavigationGui::~NavigationGui()
{

}

void NavigationGui::go_to_rental_menu()
{
	emit go_to_rental_clicked();
}

void NavigationGui::go_to_settings_menu()
{
	emit go_to_settings_clicked();
}
