Pliki gier mogą być zapisywane w formacie jpg lub png.
Nazwa gry musi pokrywać się z nazwą wpisaną do pliku wsadowego (.csv).
Wielkość liter nie ma znaczenia tzn. Azul = azul = aZuL = AZUL
Wszystkie nazwy tego typu zostaną potraktowane tak samo.
Należy jedynie zwracać uwagę na to by nazwy były na pewno takie same tzn.:
"teRaformacja marsa"  to nie to samo co "teRRaformacja marsa"
Wszystkie zdjęcia gier powinny znajdować się w tym folderze (games).
Pliki "nofoto" oraz "nogamefoto" są wykorzystywane pod taką nazwą w programie,
ich usunięcie spowoduje brak obrazów w programie. Można zastąpić je innymi grafikiami,
należy pamiętać o podaniu takie samej nazwy.

W przypadku wybierania zdjęć najlepiej wybrać takie o minimalnej rozdzielczości 450 x 450,
w przypadku większych program dostosuje wymiary więc nie ma potrzeby samemu zmienać rozmiaru obrazu.